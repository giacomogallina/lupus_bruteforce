use rayon::prelude::*;

struct IntSeq {
    seq: Vec<u8>,
    sum: u8,
    next_res: Option<Vec<u8>>,
}

impl IntSeq {
    fn new(sum: u8, length: u8) -> Self {
        let mut seq = vec![0; length as usize];
        seq[0] = sum;
        let next_res = Some(seq.clone());
        IntSeq { seq, sum, next_res }
    }
}

impl Iterator for IntSeq {
    type Item = Vec<u8>;
    fn next(&mut self) -> Option<Vec<u8>> {
        let res = self.next_res.clone();
        if self.seq[self.seq.len() - 1] != self.sum {
            for i in 0..self.seq.len()-1 {
                if self.seq[i] != 0 {
                    let t = self.seq[i];
                    self.seq[i] = 0;
                    self.seq[0] = t-1;
                    self.seq[i+1] += 1;
                    break;
                }
            }
            self.next_res = Some(self.seq.clone());
        } else {
            self.next_res = None;
        }
        res
    }
}

fn main() {
    let (count, sum): (u64, Vec<u64>) = (0..18).collect::<Vec<u8>>().par_iter().map(|bm| {
        let mut count: u64 = 0;
        let mut sum: Vec<u64> = vec![0; 25];
        for b in 0..(17-bm)/2+1 {
            println!("bm = {}, b = {}", bm, b);
            for bmv in IntSeq::new(*bm, 7) {
                for bv in IntSeq::new(b, 9) {
                    for nmv in IntSeq::new(b, 3) {
                        for nv in IntSeq::new(17-*bm-2*b, 6) {
                            let div = bmv[0];
                            let eso = bmv[1];
                            let fat = bmv[2];
                            let mag = bmv[3];
                            let mes = bmv[4];
                            let neg = bmv[5] + 2; let veg = bmv[6];
                            let alc = bv[0];
                            let con = bv[1];
                            let esp = bv[2];
                            let gua = bv[3];
                            let inv = bv[4];
                            let mas = bv[5];
                            let spi = bv[6];
                            let sta = bv[7];
                            let voy = bv[8];
                            let dia = nmv[0];
                            let sci = nmv[1];
                            let stg = nmv[2];
                            let ass = nv[0];
                            let cac = nv[1];
                            let fan = nv[2];
                            let lup = nv[3] + 2;
                            let seq = nv[4];
                            let tra = nv[5];
                            if (21 - fan - con - cac - mas - mes - tra) <= 15 {
                                let mut c3 = 0;
                                for i in 0..7 {
                                    if i == 5 && bmv[i] == 1 {
                                        c3 += 1;
                                    } else if bmv[i] == 3 {
                                        c3 += 1;
                                    }
                                }
                                for i in &bv {
                                    if *i == 3 {
                                        c3 += 1;
                                    }
                                }
                                for i in &nmv {
                                    if *i == 3 {
                                        c3 += 1;
                                    }
                                }
                                for i in 0..6 {
                                    if i == 3 && nv[i] == 1 {
                                        c3 += 1;
                                    } else if nv[i] == 3 {
                                        c3 += 1;
                                    }
                                }
                                if c3 == 1 {
                                    let mut cl = 0;
                                    if alc + ass == 1 {
                                        cl += 1;
                                    }
                                    if cac + con == 1 {
                                        cl += 1;
                                    }
                                    if dia + div == 1 {
                                        cl += 1;
                                    }
                                    if eso + esp == 1 {
                                        cl += 1;
                                    }
                                    if fan + fat == 1 {
                                        cl += 1;
                                    }
                                    if gua == 1 {
                                        cl += 1;
                                    }
                                    if inv == 1 {
                                        cl += 1;
                                    }
                                    if mag + mas + mes == 1 {
                                        cl += 1;
                                    }
                                    if sci + seq + spi + sta + stg == 1 {
                                        cl += 1;
                                    }
                                    if tra == 1 {
                                        cl += 1;
                                    }
                                    if veg + voy == 1 {
                                        cl += 1;
                                    }
                                    if cl <= 1 {
                                        let faz_p = cac + con + div + eso + esp
                                                  + gua + inv + mag + mas + mes
                                                  + sci + spi + sta + tra + veg
                                                  + voy;
                                        let faz_n = neg + fan;
                                        let faz_l = alc + ass + dia + fat + lup
                                                  + seq + stg;
                                        if 8 < faz_p && faz_p < 15 {
                                            if 3 < faz_l && faz_l < 8 {
                                                if faz_n < 5 {
                                                    if mas != 1 {
                                                        count += 1;
                                                        sum[0] += div as u64;
                                                        sum[1] += eso as u64;
                                                        sum[2] += fat as u64;
                                                        sum[3] += mag as u64;
                                                        sum[4] += mes as u64;
                                                        sum[5] += neg as u64;
                                                        sum[6] += veg as u64;
                                                        sum[7] += alc as u64;
                                                        sum[8] += con as u64;
                                                        sum[9] += esp as u64;
                                                        sum[10] += gua as u64;
                                                        sum[11] += inv as u64;
                                                        sum[12] += mas as u64;
                                                        sum[13] += spi as u64;
                                                        sum[14] += sta as u64;
                                                        sum[15] += voy as u64;
                                                        sum[16] += dia as u64;
                                                        sum[17] += sci as u64;
                                                        sum[18] += stg as u64;
                                                        sum[19] += ass as u64;
                                                        sum[20] += cac as u64;
                                                        sum[21] += fan as u64;
                                                        sum[22] += lup as u64;
                                                        sum[23] += seq as u64;
                                                        sum[24] += tra as u64;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        (count, sum)
    }).reduce(|| (0, vec![0; 25]), |(c, s), (count, sum)| {
        (c + count,
         s.iter().zip(sum.iter()).map(|(a, b)| a+b).collect())
    });
    println!("{}", count);
    let avg = sum.iter().map(|n| *n as f64 / count as f64).collect::<Vec<f64>>();
    println!("div: {}", avg[0]);
    println!("eso: {}", avg[1]);
    println!("fat: {}", avg[2]);
    println!("mag: {}", avg[3]);
    println!("mes: {}", avg[4]);
    println!("neg: {}", avg[5]);
    println!("veg: {}", avg[6]);
    println!("alc: {}", avg[7]);
    println!("con: {}", avg[8]);
    println!("esp: {}", avg[9]);
    println!("gua: {}", avg[10]);
    println!("inv: {}", avg[11]);
    println!("mas: {}", avg[12]);
    println!("spi: {}", avg[13]);
    println!("sta: {}", avg[14]);
    println!("voy: {}", avg[15]);
    println!("dia: {}", avg[16]);
    println!("sci: {}", avg[17]);
    println!("stg: {}", avg[18]);
    println!("ass: {}", avg[19]);
    println!("cac: {}", avg[20]);
    println!("fan: {}", avg[21]);
    println!("lup: {}", avg[22]);
    println!("seq: {}", avg[23]);
    println!("tra: {}", avg[24]);
}
